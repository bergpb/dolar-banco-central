.PHONY: up down logs build

COMPOSE_FILE := docker-compose-dev.yml
NAME         := bergpb/scrapy-dolar-banco-central
IMG          := ${NAME}:${TAG}
LATEST       := ${NAME}:latest


up:
	@docker compose -f $(COMPOSE_FILE) up -d

down:
	@docker compose -f $(COMPOSE_FILE) down

logs:
	@docker compose -f $(COMPOSE_FILE) logs -f

build:
	@docker build . -t ${NAME}:${TAG}
	@docker tag ${IMG} ${LATEST}
