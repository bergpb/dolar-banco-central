require 'sinatra'

set :run, false
set :environment, ENV['RACK_ENV'] || 'development'

require './src/app'
run Sinatra::Application