require 'nokogiri'
require 'open-uri'

def get_data
    html = URI.open('https://ptax.bcb.gov.br/ptax_internet/consultarUltimaCotacaoDolar.do')
    doc = Nokogiri::HTML.parse(html)

    data = doc.css("tr.fundoPadraoBClaro2 td")

    {
        date: data[0].text,
        purchase_tax: data[1].text,
        sale_tax: data[2].text
    }
end
