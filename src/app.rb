require 'sinatra'
require './src/scrapy'

set :bind, '0.0.0.0'


get "/" do
    content_type :json

    data = get_data()

    data.to_json
end
