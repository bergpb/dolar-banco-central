### Dólar Hoje - Banco Central

Scrapy que retorna fechamento de cotação de dólar através do Banco Central

Instructions:

  1. ```docker``` and ```docker compose``` are required
  2. ```$docker compose -f docker-compose-dev.yml up -d```
  3. localhost:5000

How to build:

  1. ```$TAG=version make build```
