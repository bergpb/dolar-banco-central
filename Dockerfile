FROM ruby:2.7.1-alpine as builder

RUN apk --update add --no-cache \
    build-base \
    libxml2-dev \
    libxslt-dev && \
    rm -rf /var/cache/apk/*

RUN mkdir -p /app
WORKDIR /app
COPY Gemfile /app
RUN bundle install

FROM ruby:2.7.1-alpine

ENV RACK_ENV production

RUN apk --update add --no-cache \
    curl && \
    rm -rf /var/cache/apk/*

RUN mkdir -p /app
WORKDIR /app
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY . /app

HEALTHCHECK CMD curl --fail http://localhost:5000 || exit 1

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "5000"]
